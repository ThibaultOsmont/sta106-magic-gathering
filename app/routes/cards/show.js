import Route from '@ember/routing/route';

export default Route.extend({
  model(params) {
    // Cette méthode est appelée uniquement si on arrive sur la route /cards/:id
    // Sans avoir cliqué sur une carte auparavant pour fournir les données.
    return this.store.findRecord('card', params.card_id)
  }
});
