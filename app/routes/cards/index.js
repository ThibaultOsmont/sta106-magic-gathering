import Route from '@ember/routing/route';

export default Route.extend({
  model(params) {
    if (params.rarity) {
      return this.store.query('card', { random: true, rarity: params.rarity })
    } else {
      return this.store.query('card', { random: true });
    }
  }
});
