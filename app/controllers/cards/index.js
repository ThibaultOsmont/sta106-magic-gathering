import Controller from '@ember/controller';
import { alias } from '@ember/object/computed';
import { computed } from '@ember/object';

export default Controller.extend({
  // refreshmodel
  queryParams: ['rarity', 'name'],
  rarity: '',
  name: '',

  cards: alias('model'),

  filteredCards: computed('cards', 'rarity', 'name', function () {
    console.log(this.random)
    return this.cards
      .filter(card => card.rarity.toUpperCase().startsWith(this.rarity.toUpperCase()))
      .filter(card => card.name.toUpperCase().includes(this.name.toUpperCase()));
  }),

});

/*
  About Query params:
  https://guides.emberjs.com/release/routing/query-params/
*/