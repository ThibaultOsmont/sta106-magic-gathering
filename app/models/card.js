import DS from 'ember-data';
const { Model } = DS;

export default Model.extend({
  // Les plus importants //
  name: DS.attr(),
  rarity: DS.attr(),
  imageUrl: DS.attr(),
  // ------------------- //

  names: DS.attr(),
  artist: DS.attr(),
  text: DS.attr(),
  rulings: DS.attr(),
  colors: DS.attr(),
  type: DS.attr(),
  types: DS.attr(),
  supertypes: DS.attr(),
  subtypes: DS.attr(),
  layout: DS.attr(),
});
