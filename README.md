# STA106 Magic Gathering

Premier projet Ember.js utilisant l'api de Magic - The Gathering

## Installation

```bash
npm install
```

## Démarrage

```bash
ember s
```


## Objectifs (pour se familiariser avec [Ember.js](https://emberjs.com/)):
- [x] Call le API
- [x] Afficher une liste de carte
- [x] Recherche par nom de carte
- [x] Filtrer par rareté
- [x] Cliquer sur une carte qui amène a une page qui affiche plus de détails avec ember-bootsrap + du beau CSS

## Dépendances supplémentaires utilisées

[ember-bootstrap](https://www.ember-bootstrap.com/#/components)

[bootstrap 4.3](https://getbootstrap.com/docs/4.3/getting-started/introduction/)

[ember-truth-helpers](https://github.com/jmurphyau/ember-truth-helpers)

## Documentation Ember.js
[API de Magic The Gathering](https://api.magicthegathering.io/v1/cards)

[Documentation de l'API](https://docs.magicthegathering.io/)

[Ember.js tutoriel](https://guides.emberjs.com/release/tutorial)

[Ember.js tutoriel 2](https://www.tutorialspoint.com/emberjs/index.htm)